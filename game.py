#!/usr/bin/python2
import pygame
import sys
import random
import copy
import os
import math
try:
    # For cx_freeze and py2exe
    import pygame._view
    import pygame.mixer
except:
    pass

from config import IMG_PATH, IMG_COLORKEY, SND_PATH, SCREEN_W, SCREEN_H
from config import TICK_LENGTH_MS, TICK_LENGTH_S
import config

from extratypes import V2
import extratypes

from level import Tile, Tiles
import level

from caches import ImageCache, images, SoundCache, sounds
import caches

from draw import draw_text

from section import Section, SectionMgr, sectionmgr

from menus import InfoSection, ControlsSection, MenuSection

from gamesection import Game, GameSection, GameEndSection

# Main

pygame.init()
pygame.mixer.init()

sounds.add_auto("sob_effect")
# Test stuff

#print(str(level.load_tiled("res/maps/moap1.tmx")))
#sys.exit(1)

# / Test stuff

modes = pygame.display.list_modes()
if modes:
    if config.fullscreen:
        real_screen = pygame.display.set_mode(modes[0], pygame.FULLSCREEN)
    else:
        if modes[0][0] - 20 >= SCREEN_W*3 and modes[0][1] - 100 >= SCREEN_H*3:
            real_screen = pygame.display.set_mode((SCREEN_W*3, SCREEN_H*3))
        elif modes[0][0] - 20 >= SCREEN_W*2 and modes[0][1] - 100 >= SCREEN_H*2:
            real_screen = pygame.display.set_mode((SCREEN_W*2, SCREEN_H*2))
        else:
            real_screen = pygame.display.set_mode((SCREEN_W*1, SCREEN_H*1))
else:
    real_screen = pygame.display.set_mode((SCREEN_W*2, SCREEN_H*2))

pygame.display.set_caption('Free Heart by Sydaen Kaepyset at GGJ13 FGJ13 Stage rules')
pygame.mouse.set_visible(0)

def make_screenbuf():
    return pygame.Surface((SCREEN_W, SCREEN_H))

def apply_screenbuf(screenbuf):
    global real_screen
    screenbuf = pygame.transform.scale(screenbuf, real_screen.get_size())
    real_screen.blit(screenbuf, (0,0))
    pygame.display.update()

# Show first image while loading
screenbuf = make_screenbuf()
screenbuf.fill((255,255,255))
image = pygame.transform.scale(images.get("leikkaus2"), (480, 245))
screenbuf.blit(image, (0,(SCREEN_H-245)//2))
draw_text(screenbuf, (130, 210), "LOADING...")
apply_screenbuf(screenbuf)

# Load sounds here after showing the first screen
sounds.add_auto("aar_effect")
sounds.add_auto("splat_effect")
sounds.add_auto("FreeHeart_HeartBeat")
sounds.add_auto("ouch_effect")
sounds.add_auto("FreeHeart_LevelMusic_A")
sounds.add_auto("FreeHeart_LevelMusic_B")
sounds.add_auto("yieppee_effect")
sounds.add_auto("yeahaa_effect")
sounds.add_auto("FreeHeart_Defibrillator")
sounds.add_auto("slurp_effect")
sounds.add_auto("imfree_effect")
sounds.add_auto("FreeHeart_MainScreen")
sounds.add_auto("FreeHeart_GameOver")
sounds.add_auto("FreeHeart_CardioBeep")
sounds.add_auto("FreeHeart_Flatline")

#sectionmgr.add(BaseSection())
#sectionmgr.add(GameSection())
sectionmgr.add(MenuSection())
#sectionmgr.add(GameSection(Game()))
#sectionmgr.add(GameEndSection(True))

fps = 0
tick_time_buf = 0
ticks = pygame.time.get_ticks()
while sectionmgr.stack and not config.do_exit:
    for event in pygame.event.get():
        sectionmgr.on_pygame_event(event)
    
    while tick_time_buf >= TICK_LENGTH_MS:
        sectionmgr.on_tick()
        tick_time_buf -= TICK_LENGTH_MS

    screenbuf = make_screenbuf()
    sectionmgr.on_draw(screenbuf)

    draw_text(screenbuf, (SCREEN_W-35,SCREEN_H-7), "FPS: "+str(int(round(fps))))
    screenbuf = pygame.transform.scale(screenbuf, real_screen.get_size())
    real_screen.blit(screenbuf, (0,0))
    pygame.display.update()
    
    ticks_new = pygame.time.get_ticks()
    loop_time = ticks_new - ticks
    #print("loop_time="+str(loop_time))
    ms_to_wait = TICK_LENGTH_MS - loop_time
    if ms_to_wait > 0:
        pygame.time.wait(int(ms_to_wait))
        loop_time = TICK_LENGTH_MS
    else:
        loop_time = ticks_new - ticks
    ticks = ticks_new
    tick_time_buf += loop_time
    fps = 1000.0 / loop_time

pygame.quit()
