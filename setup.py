#!/usr/bin/python2
from distutils.core import setup, Extension
import sys
import os
import glob

if "py2exe" in sys.argv:
	try:
		import py2exe
		origIsSystemDLL = py2exe.build_exe.isSystemDLL
		def isSystemDLL(pathname):
			if os.path.basename(pathname).lower() in ("msvcp71.dll", "dwmapi.dll", "libogg-0.dll"):
					return 0
			return origIsSystemDLL(pathname)
		py2exe.build_exe.isSystemDLL = isSystemDLL
	except:
		print("WARNING: Couldn't import py2exe")

data_files = []

def add_dir(src_dir, dst_dir):
    global data_files
    subdirs = []
    files = []
    for sub in glob.glob(os.path.join(src_dir, "*")):
        sub = os.path.basename(sub)
        if os.path.isdir(os.path.join(src_dir, sub)):
            subdirs.append(sub)
        else:
            files.append(sub)
    for subdir in subdirs:
        add_dir(os.path.join(src_dir, subdir), os.path.join(dst_dir, subdir))
    src_filepaths = []
    for fname in files:
        src_filepaths.append(os.path.join(src_dir, fname))
    print("add_dir(): "+dst_dir+": "+str(len(src_filepaths))+" files")
    data_files.append((dst_dir, src_filepaths))

def add_file(src_file, dst_dir):
    global data_files
    data_files.append((dst_dir, [src_file]))

if os.name == "nt":
    add_dir("res", "res")
else:
    sys.stderr.write("Platform '"+sys.platform+"' not supported.\n")
    exit(1)

setup(
    name = 'freeheart',
    version = '0',
    data_files = data_files,
    ext_modules = [],
    packages = [],
    scripts = ['game.py'],
    options = {
        'py2exe': {
            'optimize': 2,
            'compressed': 2,
            'bundle_files': 1,
            'dist_dir': 'freeheart_win_py2exe',
        }
    },
    windows = [{
        'script': 'game.py',
    }],
    zipfile = None, # This places the python zip library into the executable
)


