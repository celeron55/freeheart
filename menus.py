import pygame

from config import IMG_PATH, IMG_COLORKEY, SND_PATH, SCREEN_W, SCREEN_H
from config import TICK_LENGTH_MS, TICK_LENGTH_S
import config

from level import Tile, Tiles
import level

from draw import draw_text

from section import Section, SectionMgr, sectionmgr

from caches import ImageCache, images, SoundCache, sounds
import caches

from gamesection import Game, GameSection, GameEndSection

class InfoSection(Section):
    def __init__(self):
        Section.__init__(self)
    def on_draw(self, screen):
        screen.fill((255,255,255))
        text = "INFORMATION"
        draw_text(screen, (SCREEN_W/2-5*len(text)//2, 32), text)
        x = 64
        y = 80
        draw_text(screen, (x, y), "FREE HEART"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "FOR GGJ/FGJ13 BY TEAM SYDAEN KAEPYSET:"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "[ORIGINAL IDEA] MIKKO LAPINLAHTI"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "         [CODE] PERTTU AHOLA"); y += 8
        draw_text(screen, (x, y), "                SEBASTIAN TURPEINEN"); y += 8
        draw_text(screen, (x, y), "                RAULI PUUPERAE"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "     [GRAPHICS] KATJA MAEAETTAE"); y += 8
        draw_text(screen, (x, y), "                JYRKI JAKONEN"); y += 8
        draw_text(screen, (x, y), "                ANTTI HOLAPPA"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "     [MUSIC&FX] MIKKO LAPINLAHTI"); y += 8
        draw_text(screen, (x, y), "                RAULI PUUPERAE"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), "[PAEAESMAEROE.] RAULI PUUPERAE"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), ""); y += 8
        return True
    def on_pygame_event(self, event):
        if event.type == pygame.QUIT:
            config.do_exit = True
        if event.type == pygame.KEYDOWN:
            self.to_be_removed = True
            return True

class ControlsSection(Section):
    def __init__(self):
        Section.__init__(self)
    def on_draw(self, screen):
        screen.fill((255,255,255))
        text = "CONTROLS"
        draw_text(screen, (SCREEN_W/2-5*len(text)//2, 32), text)
        x = 64
        y = 80
        draw_text(screen, (x, y), " [LEFT] ROTATE TARGET ANGLE"); y += 8
        draw_text(screen, (x, y), "[RIGHT]"); y += 8
        draw_text(screen, (x, y), ""); y += 8
        draw_text(screen, (x, y), " [DOWN] RESET TARGET ANGLE"); y += 8
        return True
    def on_pygame_event(self, event):
        if event.type == pygame.QUIT:
            config.do_exit = True
        if event.type == pygame.KEYDOWN:
            self.to_be_removed = True
            return True

class MenuSection(Section):
    def __init__(self):
        Section.__init__(self)
    	self.bgimage = pygame.Surface((SCREEN_W, SCREEN_H))
        self.bgimage.fill((255,255,255))
        image = pygame.transform.scale(images.get("leikkaus"), (480, 245))
        self.bgimage.blit(image, (0,(SCREEN_H-245)//2))
        self.song = sounds.get("FreeHeart_MainScreen").play()
    def on_draw(self, screen):
        screen.blit(self.bgimage, (0,0))
        text = "FREE HEART"
        #draw_text(screen, (SCREEN_W/2-5*len(text)//2, 30), text)
        draw_text(screen, (130, 180), text)
        x = 130
        y = 210
        text = "[SPACE] START GAME"
        draw_text(screen, (x, y), text)
        text = "    [I] INFORMATION"
        draw_text(screen, (x, y+8), text)
        text = "    [C] CONTROLS"
        draw_text(screen, (x, y+16), text)
        text = "  [ESC] QUIT"
        draw_text(screen, (x, y+24), text)
        return True
    def on_pygame_event(self, event):
        if event.type == pygame.QUIT:
            config.do_exit = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE or event.key == pygame.K_q:
                self.to_be_removed = True
            if event.key == pygame.K_SPACE or event.key == pygame.K_RETURN:
                full_level = level.load_tiled("res/maps/Level1.tmx")
                self.song.stop()
                sounds.get("imfree_effect").play()
                sectionmgr.add(GameSection(Game(full_level)))
            if event.key == pygame.K_i:
                sectionmgr.add(InfoSection())
            if event.key == pygame.K_c:
                sectionmgr.add(ControlsSection())
            return True


