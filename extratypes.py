# Copyright 2012 Perttu Ahola <celeron55@gmail.com>; All rights reserved.

from math import sqrt

def limit(x, min_, max_):
    if x < min_: return min_
    if x > max_: return max_
    return x
def wrap(x, min_, max_):
    while x < min_: x += (max_ - min_)
    while x > max_: x -= (max_ - min_)
    return x

class V2(object):
    
    def __init__(self, x, y):
        self.x = x
        self.y = y

    def __iter__(self):
        return iter((self.x, self.y))

    def dot(self, other):
        return self.x*other.x + self.y*other.y

    def __add__(self, other):
        return V2(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return V2(self.x - other.x, self.y - other.y)

    def __mul__(self, scalar):
        return V2(self.x*scalar, self.y*scalar)

    def __div__(self, scalar):
        return V2(self.x/scalar, self.y/scalar)

    def __floordiv__(self, scalar):
        return V2(self.x//scalar, self.y//scalar)

    #def __eq__(self, other):
    #    return (self.x == other.x and self.y == other.y)
    
    #"""
    def __cmp__(self, other):
        #if self.__eq__(other):
        if (self.x == other.x and self.y == other.y):
            return 0
        if ((self.x < other.x and self.x != other.x) or
            (self.x == other.x and self.y < other.y and self.y != other.y)):
            return -1
        return 1
    #"""

    def __hash__(self):
        return self.y*10000 + self.x

    @property
    def length(self):
        return sqrt(self.x*self.x + self.y*self.y)

    @property
    def normal(self):
        return self * (1.0/self.length)

    @property
    def i(self):
        return V2(int(self.x),int(self.y))

    @property
    def xy(self):
        return self.x, self.y

    def __str__(self):
        return "("+str(self.x)+","+str(self.y)+")"

    def __repr__(self):
        return "V2("+str(self.x)+","+str(self.y)+")"

    def copy(self):
        return V2(self.x, self.y)

