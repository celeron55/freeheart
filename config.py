import sys

do_exit = False
fullscreen = False
if len(sys.argv) == 2 and sys.argv[1] == "fs":
    fullscreen = True

IMG_PATH = 'res'
IMG_COLORKEY = (255,0,255)

SND_PATH = 'res/sounds'

SCREEN_W = 480
SCREEN_H = 300

TICK_LENGTH_MS = 1000/30
TICK_LENGTH_S = TICK_LENGTH_MS/1000.0

TILE_W = 20
TILE_H = 20

