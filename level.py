import pygame

from caches import ImageCache, images, SoundCache, sounds
import caches

from config import IMG_PATH, IMG_COLORKEY, SND_PATH, SCREEN_W, SCREEN_H
from config import TICK_LENGTH_MS, TICK_LENGTH_S, TILE_W, TILE_H
import config

class TileInfo:
    def __init__(self):
        self.image = None
        self.image_x = 0
        self.image_y = 0
        self.solid = False
        self.spike = False
        self.hp = 0
        self.speed = 0

class Tile:
    def __init__(self, t=-1, t2=-1):
        self.t = t
        self.t2 = t2

class Tiles:
    class InvalidPositionError(Exception):
        pass
    def __init__(self, x0, y0, w, h):
        self.x0 = x0
        self.y0 = y0
        self.w = w
        self.h = h
        self.tiles = []
        for i in range(0,w*h):
            self.tiles.append(Tile())
    def ref(self, x, y):
        if (y < self.y0 or y >= self.y0 + self.h or
                x < self.x0 or x >= self.x0 + self.w):
            raise Tiles.InvalidPositionError()
        i = (y - self.y0) * self.w + (x - self.x0)
        try:
            return self.tiles[i]
        except:
            raise Tiles.InvalidPositionError()
    def __str__(self):
        return "Tiles x0="+str(self.x0)+" y0="+str(self.y0)+" w="+str(self.w)+" h="+str(self.h)

class LevelObject:
    def __init__(self, name="unknown", x=0, y=0, w=0, h=0, properties=None):
        self.name = name
        self.x = x
        self.y = y
        self.w = w
        self.h = h
        self.properties = properties
        if not self.properties:
            self.properties = {}

class FullLevel:
    def __init__(self, tileinfos, tiles, objects):
        self.tileinfos = tileinfos
        self.tiles = tiles
        self.objects = objects
    def __str__(self):
        return "tileinfos="+str(self.tileinfos)+" tiles="+str(self.tiles)

from xml.dom import minidom
import zlib
import base64

class Tileset:
    def __init__(self):
        self.firstgid = 0
        self.image = None
        self.image_tw = None # Width in tiles

# Loads a tiled level from path (a tmx file)
# Returns FullLevel
def load_tiled(path):
    xdoc = minidom.parse(path)
    xtilesets = xdoc.getElementsByTagName('tileset')
    tileinfos = {}
    tileinfos[-1] = TileInfo()
    #tileinfos[-1].solid = True
    tilesets = []
    for xtileset in xtilesets:
        name = "unknown"
        try:
            name = xtileset.attributes["name"].value
        except:
            pass
        firstgid = int(xtileset.attributes["firstgid"].value)
        ximages = xtileset.getElementsByTagName("image")
        if len(ximages) == 0:
            continue
        img_rel_path = ximages[0].attributes["source"].value
        imagepath = "res/maps/" + img_rel_path
        tw = int(xtileset.attributes["tilewidth"].value)
        th = int(xtileset.attributes["tileheight"].value)
        print("tileset \""+name+"\": tw="+str(tw)+" th="+str(th)
                +" firstgid="+str(firstgid)+", imgagepath="+imagepath)
        tileset_img = images.get(imagepath)
        tileset_img_tw = tileset_img.get_width() / TILE_W
        # Add tileset
        tileset = Tileset()
        tileset.firstgid = firstgid
        tileset.image = tileset_img
        tileset.image_tw = tileset_img_tw
        tilesets.append(tileset)
        # Get tile properties
        xtiles = xtileset.getElementsByTagName('tile')
        for xtile in xtiles:
            tileid = firstgid + int(xtile.attributes["id"].value)
            tileinfo = TileInfo()
            tileinfo.image = tileset.image
            image_i = int(xtile.attributes["id"].value)
            tileinfo.image_x = image_i % tileset.image_tw
            tileinfo.image_y = int(image_i / tileset.image_tw)
            xproperties = xtile.getElementsByTagName("property")
            for xproperty in xproperties:
                if xproperty.attributes["name"].value == "solid":
                    tileinfo.solid = (xproperty.attributes["value"].value == "true")
                if xproperty.attributes["name"].value == "spike":
                    tileinfo.spike = True
                if xproperty.attributes["name"].value == "hp":
                    tileinfo.hp = int(xproperty.attributes["value"].value)
                if xproperty.attributes["name"].value == "speed":
                    tileinfo.speed = int(xproperty.attributes["value"].value)
            print("- tile: "+str(tileid)+" solid="+str(tileinfo.solid)+" hp="+str(tileinfo.hp)+" speed="+str(tileinfo.speed))
            tileinfos[tileid] = tileinfo
    tiles = None
    xlayers = xdoc.getElementsByTagName('layer')
    for xlayer in xlayers:
        name = xlayer.attributes["name"].value
        print("Layer \""+name+"\"")
        w = int(xlayer.attributes['width'].value)
        h = int(xlayer.attributes['height'].value)
        print("w="+str(w)+" h="+str(h))
        if tiles is None:
            print("Creating tiles of size "+str(w)+"x"+str(h))
            tiles = Tiles(0, 0, w, h)
        xdata = xlayer.getElementsByTagName("data")[0]
        tiledata = zlib.decompress(base64.b64decode(xdata.childNodes[0].data))
        for i in range(0,len(tiledata)/4):
            gid = (ord(tiledata[i*4+0]) + (ord(tiledata[i*4+1])<<8) +
                    (ord(tiledata[i*4+2])<<16) + (ord(tiledata[i*4+3])<<16))
            # Find the highest firstgid that is lower than num
            tileset = None
            for tileset0 in tilesets:
                if (tileset is None or
                        (tileset0.firstgid >= tileset.firstgid and
                        tileset0.firstgid <= gid)):
                    tileset = tileset0
            num = gid - tileset.firstgid
            # Set tile in memory
            if name == "background":
                tiles.ref(i%w, int(i/w)).t = gid
            elif name == "foreground":
                tiles.ref(i%w, int(i/w)).t2 = gid
            # Add tileinfo if doesn't already exist (if it didn't have properties)
            if gid not in tileinfos:
                tileinfo = TileInfo()
                if gid != 0:
                    tileinfo.image = tileset.image
                    image_i = num
                    tileinfo.image_x = image_i % tileset.image_tw
                    tileinfo.image_y = int(image_i / tileset.image_tw)
                tileinfos[gid] = tileinfo
    objects = []
    xobjects = xdoc.getElementsByTagName('object')
    for xobject in xobjects:
        name = xobject.attributes["name"].value
        x = int(xobject.attributes["x"].value)
        y = int(xobject.attributes["y"].value)
        w = 0
        h = 0
        try: # Some objects don't have width/height
            w = int(xobject.attributes["width"].value)
            h = int(xobject.attributes["height"].value)
        except:
            pass
        properties = {}
        for xproperty in xobject.getElementsByTagName("property"):
            vname = xproperty.attributes["name"].value
            value = xproperty.attributes["value"].value
            print("Object \""+name+"\" has property \""+vname+"\" = \""+value+"\"")
            properties[vname] = value
        objects.append(LevelObject(name, x,y,w,h, properties))
    # Print stuff about result
    #tohex = lambda x:":".join([hex(ord(c))[2:].zfill(2) for c in x])
    #print("tiledata="+tohex(tiledata))
    for ty in range(tiles.y0, tiles.y0 + tiles.h):
        rowids = []
        for tx in range(tiles.x0, tiles.x0 + tiles.w):
            try:
                tt = tiles.ref(tx, ty).t
                rowids.append(str(tt).rjust(3, ' '))
            except:
                pass
        print(",".join(rowids))
    print("Loaded tiles:")
    for tileid, tileinfo in tileinfos.items():
        # Hack: If something is spike, make it not solid
        if tileinfo.spike:
            tileinfo.solid = False
        print(" "+str(tileid)+": image="+str(tileinfo.image)+", image_x/y="+str(tileinfo.image_x)+","+str(tileinfo.image_y)+", solid="+str(tileinfo.solid))
    return FullLevel(tileinfos, tiles, objects)

