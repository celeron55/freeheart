import pygame

from caches import ImageCache, images, SoundCache, sounds
import caches

def draw_text(screen, pos, text):
    for char in text:
        ic = ord(char)
        iy = ic // 16
        ix = ic - iy * 16
        screen.blit(images.get("font"), pos, pygame.Rect(ix*5, iy*8, 5, 8))
        pos = (pos[0]+5, pos[1])

