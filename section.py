class Section:
    def __init__(self):
        self.to_be_removed = False
    def on_draw(self, screen):
        pass
    def on_pygame_event(self, event):
        pass
    def on_tick(self):
        pass

class SectionMgr:
    def __init__(self):
        self.stack = []
    def add(self, section):
        self.stack.append(section)
    def remote(self, section):
        self.stack.remove(section)
    def on_draw(self, screen):
        for section in reversed(self.stack):
            if section.on_draw(screen):
                break
    def on_pygame_event(self, event):
        for section in reversed(self.stack):
            if section.on_pygame_event(event):
                break
    def on_tick(self):
        for section in self.stack:
            if section.to_be_removed:
                self.stack.remove(section)
                continue
            if section.on_tick():
                break

sectionmgr = SectionMgr()


