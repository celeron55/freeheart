import pygame
from config import IMG_PATH, IMG_COLORKEY, SND_PATH, SCREEN_W, SCREEN_H
from config import TICK_LENGTH_MS, TICK_LENGTH_S

class ImageCache:
    def __init__(self, img_path):
        self.images = {}
        self.img_path = img_path
    def get(self, name, scale=1.0):
        if name in self.images:
            if scale in self.images[name]:
                return self.images[name][scale]
        if ".bmp" in name:
            path = name
        else:
            path = self.img_path + '/' + name + '.bmp'
        if name not in self.images:
            self.images[name] = {}
        #image = pygame.image.load(path).convert_alpha()
        image = pygame.image.load(path).convert()
        s = image.get_size()
        s = (int(s[0]*scale), int(s[1]*scale))
        image = pygame.transform.scale(image, s)
        self.images[name][scale] = image
        self.images[name][scale].set_colorkey(IMG_COLORKEY)
        return self.images[name][scale]

images = ImageCache(IMG_PATH)

class SoundCache:
    def __init__(self):
        self.sounds = {}
    def add(self, name, sound):
        self.sounds[name] = sound
    def add_auto(self, name, volume=1.0):
        sound = pygame.mixer.Sound(SND_PATH + '/' + name + '.ogg')
        sound.set_volume(volume)
        self.add(name, sound)
    def get(self, name):
        if name in self.sounds:
            return self.sounds[name]
        class SoundNotFoundException(Exception):
            pass
        print("Sound not found: " + name)
        raise SoundNotFoundException()
    def stop_all(self):
        for name, sound in self.sounds.items():
            sound.stop()

sounds = SoundCache()


