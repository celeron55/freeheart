import math
import pygame
import random

from config import IMG_PATH, IMG_COLORKEY, SND_PATH, SCREEN_W, SCREEN_H
from config import TICK_LENGTH_MS, TICK_LENGTH_S, TILE_W, TILE_H
import config

from level import Tile, Tiles
import level

from draw import draw_text

from section import Section, SectionMgr, sectionmgr

from caches import ImageCache, images, SoundCache, sounds
import caches

from extratypes import V2

import math

ONGROUND = 0
FLYING = 1
GRAVITY = V2(0, 0.003)

COLLISION_TOP = -4
COLLISION_BOTTOM = 9

END_WAIT_S = 3.0
FAIL_END_WAIT_S = 3.0
GAME_END_WAIT_S = 3.0

class Heart:
    def __init__(self):
        #self.image = pygame.Surface((20, 20))
        #self.image.fill((255,0,0))
        self.image = images.get("heart")
        self.p = V2(10, SCREEN_H-60)
        self.last_land_p = V2(self.p.x, self.p.y)
        self.state = FLYING
        self.prev_state = self.state
        self.velocity = V2(0.2, -0.3)
        # This is used to get on top of surfaces when jumping against wall
        self.wanted_direction = 0
        self.last_beat = 0
        self.angle = 90.0
        self.energy = 100
        self.rate_factor = 1.0
        self.air_ticks = 0 # Ticks into a jump
        self.jump_cost = 5
        self.invulnerability_ticks = 0 # If >0, is invulnerable; counts down in ticks
    
    def set_pos(self, p):
        self.p = V2(p.x, p.y)
        self.last_land_p = V2(self.p.x, self.p.y)
        self.state = FLYING

    def rate(self):
        return 25 + 0.25 * self.energy * self.rate_factor

    def beatit(self, angle=None):
        if not angle:
            angle = self.angle

        angle = math.radians(angle)
        self.velocity = V2(math.cos(angle), -math.sin(angle)) * 1.3
        if angle > 90:
            self.wanted_direction = -1
        else:
            self.wanted_direction = 1
        self.state = FLYING
        self.air_ticks = 0

        self.energy -= self.jump_cost

    def phystick(self):
        if self.state == FLYING:
            self.air_ticks += 1
        else:
            self.air_ticks = 0
        #print("air_ticks="+str(self.air_ticks))
    def tick(self):
        if self.invulnerability_ticks > 0:
            self.invulnerability_ticks -= 1
    
    def hurt(self, amount):
        if self.invulnerability_ticks > 0:
            return
        self.energy -= amount
        self.invulnerability_ticks = 1.0 / TICK_LENGTH_S

class Entity:
    def __init__(self):
        self.p = V2(0,0) # Position of center bottom
        self.bottom_y = 200
        self.move_min_x = 0
        self.move_max_x = 100
        self.frame_count = 2
        self.frame_w = 40
        self.image_left = images.get("doctor")
        self.image_right = pygame.transform.flip(images.get("doctor"), True, False)
        self.direction = -1
        self.speed = 1 # Pixels per tick
        self.ticks = 0
    def tick(self):
        self.ticks += 1
        self.p.y = self.bottom_y
        self.p.x += self.direction * self.speed
        if self.direction == -1 and self.p.x < self.move_min_x + self.frame_w//2:
            self.direction *= -1
        elif self.direction == 1 and self.p.x > self.move_max_x - self.frame_w//2:
            self.direction *= -1

        if self.p.x < self.move_min_x - self.speed:
            self.p.x = self.move_min_x
        elif self.p.x > self.move_max_x + self.speed:
            self.p.x = self.move_max_x
    def get_frame_src_rect(self):
        sprite_i = int(self.ticks // 3)%self.frame_count
        sprite_tx = sprite_i % 10
        sprite_ty = int(sprite_i / 10)
        frame_w = self.frame_w
        frame_h = self.image_left.get_height()
        src_rect = pygame.Rect(sprite_tx*frame_w,sprite_ty*frame_h, frame_w,frame_h)
        return src_rect
    def get_frame_dst_rect(self):
        rect = self.image_left.get_rect()
        rect = pygame.Rect(rect.left, rect.top,
                self.frame_w, rect.height)
        return rect
    def get_image(self):
        if self.direction < 0:
            return self.image_left
        else:
            return self.image_right

class Particle:
    def __init__(self, p=None, v=None, color=None):
        self.p = p
        if self.p is None:
            self.p = V2(0,0)
        self.v = v
        if self.v is None:
            self.v = V2(0,0)
        self.color = color
        if self.color is None:
            self.color = (255,0,0)

class Game:
    def __init__(self, full_level):
        self.tileinfos = full_level.tileinfos
        self.tiles = full_level.tiles
        self.heart = Heart()
        self.entities = []
        #self.entities.append(Entity()) # Test
        self.particles = []
        #self.particles.append(Particle(V2(100,150), V2(1,0), (255,0,0)))
        #self.particles.append(Particle(V2(100,152), V2(1,0), (255,0,0)))
        #self.particles.append(Particle(V2(100,154), V2(1,0), (255,0,0)))

        self.end_area = pygame.Rect(0,0,0,0)
        self.next_level = None

        self.end_launch_timer = -1 # Set to positive when ended, when goes to 0, level ends
        self.level_ended = False
        self.won = True

        self.song = random.choice([sounds.get("FreeHeart_LevelMusic_A"), sounds.get("FreeHeart_LevelMusic_B")])
        self.song.play()

        for obj in full_level.objects:
            if obj.name == "start":
                p = V2(obj.x, obj.y)
                print("\"start\" object supplied; position: "+str(p))
                self.heart.set_pos(p)
            if obj.name == "end":
                print("\"end\" object supplied")
                self.end_area = pygame.Rect(obj.x, obj.y, obj.w, obj.h)
                self.next_level = obj.properties["next"]
            if obj.name == "doctor":
                print("\"doctor\" object supplied")
                entity = Entity()
                entity.bottom_y = obj.y + obj.h
                entity.move_min_x = obj.x
                entity.move_max_x = obj.x + obj.w
                entity.frame_w = 40
                entity.image_left = images.get("doctor")
                entity.image_right = pygame.transform.flip(images.get("doctor"), True, False)
                self.entities.append(entity)

    def get_entity_at(self, x, y):
        for entity in self.entities:
            dst_rect = entity.get_frame_dst_rect()
            dst_rect = dst_rect.move(int(entity.p.x)-entity.frame_w//2,
                    int(entity.p.y)-entity.image_left.get_height())
            if dst_rect.collidepoint(x, y):
                return entity
        return None

    def get_tile_at(self, x, y):
        tx = int(x // TILE_W)
        ty = int(y // TILE_H)
        return self.tiles.ref(tx, ty)
    def is_solid_at(self, x, y):
        t = self.get_tile_at(x, y)
        tt = t.t
        tt2 = t.t2
        if self.tileinfos[tt].solid:
            return True
        if self.tileinfos[tt2].solid:
            return True
        return False
    def handle_powerups_at(self, x, y):
        t = self.get_tile_at(x, y)
        took_something = False
        # Spikes
        if self.tileinfos[t.t2].spike:
            self.heart.hurt(15)
            sounds.get('ouch_effect').play()
            self.emit_blood_particles(self.heart.p)
            if self.heart.velocity.y > 1:
                self.heart.velocity.y *= -1
            else:
                self.heart.velocity.y = -1
            self.heart.velocity.x *= 0.754
        # Energy
        if self.tileinfos[t.t2].hp != 0:
            print("got hp")
            self.heart.energy += self.tileinfos[t.t2].hp
            print("new energy: "+str(self.heart.energy))
            sounds.get("slurp_effect").play()
            took_something = True
        if self.heart.energy > 100:
            self.heart.energy = 100
        if self.heart.energy < 0:
            self.heart.energy = 0
        # Speed
        if self.tileinfos[t.t2].speed != 0:
            speed = self.tileinfos[t.t2].speed
            print("got speed: "+str(speed))
            self.heart.rate_factor *= (100.0 + speed) / 100.0
            print("new rate factor: "+str(self.heart.rate_factor))
            #FIXME ugly hack
            if speed == 2:
                sounds.get("FreeHeart_Defibrillator").play()
            else:
                sounds.get("yeahaa_effect").play()
            took_something = True
            self.heart.jump_cost = 3
        # Remove tile
        if took_something:
            t.t2 = 0

    def emit_blood_particles(self, p):
        for i in range(0,10):
            self.particles.append(Particle(
                    V2(random.randint(-8,8)+p.x,random.randint(3,8)+p.y),
                    V2(random.randint(-3,3),random.randint(-4,2)),
                    (255,0,0)))
        if len(self.particles) > 30:
            self.particles = self.particles[20:]

    def phystick(self, dtime_ms):
        heart = self.heart
        heart.prev_state = heart.state
 
        if pygame.key.get_pressed()[pygame.K_LEFT]:
            heart.angle += 0.1 * dtime_ms
        elif pygame.key.get_pressed()[pygame.K_RIGHT]:
            heart.angle -= 0.1 * dtime_ms
        elif pygame.key.get_pressed()[pygame.K_DOWN]:
            heart.angle = 90.0

        if heart.state == ONGROUND:
            heart.last_land_p = V2(heart.p.x, heart.p.y)
            heart.velocity.x = 0
            ticks = pygame.time.get_ticks()
            if ticks > heart.last_beat + (60000/heart.rate()):
                sounds.get("FreeHeart_HeartBeat").play()
                sounds.get("FreeHeart_CardioBeep").play()
                
                heart.angle = int(heart.angle)
                heart.last_beat = ticks
                heart.beatit()

        elif heart.state == FLYING:
            # Regain a bit of horizontal speed to get better on things
            if heart.velocity.x == 0:
                heart.velocity.x = 0.1 * heart.wanted_direction

        # Y-wise movement with collision detection
        try:
            newp = V2(heart.p.x, heart.p.y + heart.velocity.y * dtime_ms)
            if (self.is_solid_at(newp.x, newp.y+COLLISION_TOP) or
                    self.is_solid_at(newp.x, newp.y+COLLISION_BOTTOM) or
                    self.is_solid_at(newp.x, newp.y)):
                heart.velocity.y = 0

                if self.is_solid_at(newp.x, newp.y+COLLISION_BOTTOM):
                    if heart.state != ONGROUND:
                        self.emit_blood_particles(heart.p)
                    heart.state = ONGROUND
            else:
                heart.p = newp
        except Tiles.InvalidPositionError as e:
            pass
        # X-wise movement with collision detection
        try:
            newp = V2(heart.p.x + heart.velocity.x * dtime_ms, heart.p.y)
            if (self.is_solid_at(newp.x, newp.y+COLLISION_TOP) or
                    self.is_solid_at(newp.x, newp.y+COLLISION_BOTTOM) or
                    self.is_solid_at(newp.x, newp.y)):
                #heart.velocity.x = 0
                pass
            elif self.get_entity_at(newp.x, newp.y):
                sounds.get('ouch_effect').play()
                self.emit_blood_particles(heart.p)
                heart.hurt(5)
                heart.velocity.x *= -1
                heart.p.y = newp.y
                heart.p.x -= (newp.x - heart.p.x)
            else:
                heart.p = newp
        except Tiles.InvalidPositionError as e:
            pass

        gravity = GRAVITY * dtime_ms
        heart.velocity += gravity
        heart.velocity = V2(heart.velocity.x, 
            heart.velocity.y + gravity.y)

        #if heart.rect.bottom > SCREEN_H:
        #   heart.rect.bottom = SCREEN_H
        #   heart.velocity = (0, 0)
        #   heart.state = ONGROUND 

        if heart.prev_state == FLYING and heart.state == ONGROUND:
            sounds.get('splat_effect').play()

        self.heart.phystick()

        # Check endpoint
        if self.end_area.collidepoint(heart.p.x, heart.p.y):
            if self.end_launch_timer < 0:
                sounds.get("yieppee_effect").play()
                print("end; going to \""+self.next_level+"\" after "+str(END_WAIT_S)+"s")
                self.end_launch_timer = END_WAIT_S / TICK_LENGTH_S
        
        # Handle powerups
        try:
            self.handle_powerups_at(newp.x, newp.y+COLLISION_TOP)
            self.handle_powerups_at(newp.x, newp.y+COLLISION_BOTTOM)
            self.handle_powerups_at(newp.x-10, newp.y)
            self.handle_powerups_at(newp.x+10, newp.y)
        except Tiles.InvalidPositionError as e:
            pass

    def tick(self):
        if self.end_launch_timer >= 0:
            if self.end_launch_timer > 0:
                self.end_launch_timer -= 1
            if self.end_launch_timer <= 0:
                self.level_ended = True
            return

        heart = self.heart
        self.heart.tick()
        # Run physics at higher tick rate to get some accuracy
        for i in range(0,10):
            self.phystick(TICK_LENGTH_MS/10.)

        # Check for death
        if self.heart.energy <= 0 and self.heart.state == ONGROUND:
            self.song.stop()
            sounds.get("sob_effect").play()
            sounds.get("FreeHeart_Flatline").play()
            sounds.get("FreeHeart_GameOver").play()
            self.end_launch_timer = FAIL_END_WAIT_S / TICK_LENGTH_S
            self.won = False
            return
        
        for particle in self.particles:
            newp = particle.p + particle.v
            try:
                if not self.is_solid_at(newp.x, newp.y):
                    particle.p = newp
            except Tiles.InvalidPositionError as e:
                pass
            particle.v.y += 0.5

    def stop(self):
        self.song.stop()

class GameSection(Section):
    def __init__(self, game=None):
        Section.__init__(self)
        self.start_game(game)
    
    def start_game(self, game):
        self.game = game or Game()
        # Position of the level on the screen
        self.level_x = 0
        self.level_y = 0

    def on_draw(self, screen):
        game = self.game
        #tileset_img = images.get("tileset")
        #tileset_img_tw = tileset_img.get_width() / TILE_W
        for tx in range(game.tiles.x0, game.tiles.x0 + game.tiles.w):
            dx = tx * TILE_W + self.level_x
            if dx + TILE_W < 0:
                continue
            if dx >= SCREEN_W:
                continue
            for ty in range(game.tiles.y0, game.tiles.y0 + game.tiles.h):
                dy = ty * TILE_H + self.level_y
                if dy + TILE_H < 0:
                    continue
                if dy >= SCREEN_H:
                    continue
                t = game.tiles.ref(tx, ty)
                try:
                    tt = t.t
                    tileinfo = game.tileinfos[tt]
                    if tileinfo.image:
                        ix = tileinfo.image_x
                        iy = tileinfo.image_y
                        rect = pygame.Rect(ix*TILE_W, iy*TILE_H, TILE_W, TILE_H)
                        screen.blit(tileinfo.image, (dx, dy), rect)
                except Tiles.InvalidPositionError as e:
                    pass
                try:
                    tt = t.t2
                    tileinfo = game.tileinfos[tt]
                    if tileinfo.image:
                        ix = tileinfo.image_x
                        iy = tileinfo.image_y
                        rect = pygame.Rect(ix*TILE_W, iy*TILE_H, TILE_W, TILE_H)
                        screen.blit(tileinfo.image, (dx, dy), rect)
                except Tiles.InvalidPositionError as e:
                    pass

        for entity in game.entities:
            src_rect = entity.get_frame_src_rect()
            dst_rect = entity.get_frame_dst_rect()
            dst_rect = dst_rect.move(int(entity.p.x)-entity.frame_w//2+self.level_x,
                    int(entity.p.y)-entity.image_left.get_height()+self.level_y)
            screen.blit(entity.get_image(), dst_rect, src_rect)

        for particle in game.particles:
            pygame.draw.rect(screen, particle.color, pygame.Rect(
                    int(particle.p.x)+int(self.level_x)-1,
                    int(particle.p.y)+int(self.level_y)-1,
                    3,3))

        #screen.blit(images.get("game"), (0, 0), bg_src_rect)
        heart = game.heart
        #heart_rect = heart.image.get_rect()
        dst_rect = pygame.Rect(0,0,20,20)
        dst_rect.center = (int(heart.p.x)+self.level_x, heart.p.y+self.level_y)
        sprite_i = heart.air_ticks / 5
        if sprite_i >= 37:
            sprite_i = 36
        sprite_tx = sprite_i % 10
        sprite_ty = int(sprite_i / 10)
        src_rect = pygame.Rect(sprite_tx*20,sprite_ty*20, 20,20)
        screen.blit(heart.image, dst_rect, src_rect)

        if heart.state != FLYING and game.end_launch_timer == -1:
            arrow = images.get("arrow2")
            arrow = pygame.transform.rotate(arrow, heart.angle)
            arrow_rect = arrow.get_rect()
            arrow_rect.center = dst_rect.center
            arrow_rect.move(0,8)
            screen.blit(arrow, arrow_rect)

        atlas = images.get("blood_transfusion")
        
        health_dst_rect = pygame.Rect(0, 0, 40, 56)
        health_dst_rect.center = (SCREEN_W-22, 28)
        
        health_sprite_id = min(9, 10 - int(round(heart.energy / 10.0)))
        health_src_rect = pygame.Rect(health_sprite_id * 40, 0, 40, 56)
        
        screen.blit(atlas, health_dst_rect, health_src_rect)

        ekg_grid = images.get("ekg_grid")

        ekg_rect = pygame.Rect(0, 0, 120, 40)
        ekg_rect.center = (65, 25)
        screen.blit(ekg_grid, ekg_rect)

        ticks_left = heart.last_beat + (60000/heart.rate()) - pygame.time.get_ticks()
        ticks_ratio = round((pygame.time.get_ticks() - heart.last_beat) / (60000 / heart.rate()), 2) 
        
        ekg = images.get('ekg')

        center_x =  (45 + (150 - 150 * ticks_ratio)) % 150

        ekg_dst_rect = pygame.Rect(0, 0, 120, 40)
        ekg_dst_rect.center = (65, 25)

        if self.game.won:
            ekg_src_rect = pygame.Rect(center_x, 0, 120, 40)
        else:
            ekg_src_rect = pygame.Rect(0, 0, 120, 40)

        screen.blit(ekg, ekg_dst_rect, ekg_src_rect)
        
        #draw_text(screen, (4,2), "TEST")
        return True

    def on_pygame_event(self, event):
        game = self.game
        if event.type == pygame.QUIT:
            self.game.stop()
            config.do_exit = True
            return True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_ESCAPE:
                self.game.stop()
                self.to_be_removed = True
            return True
        if event.type == pygame.KEYUP:
            return True

    def on_tick(self):
        global sectionmgr
        game = self.game
        keys = pygame.key.get_pressed()
        game.accel_pedal = keys[pygame.K_UP]
        game.tick()
        # Move viewpoint
        target_level_x = -game.heart.p.x + SCREEN_W/2
        if game.tiles.h > 15:
            target_level_y = -game.heart.last_land_p.y + SCREEN_H/6 + SCREEN_H/2
        else:
            target_level_y = 0
        self.level_x = self.level_x * 0.8 + target_level_x * 0.2
        self.level_y = self.level_y * 0.9 + target_level_y * 0.1
        
        for entity in game.entities:
            entity.tick()

        if game.level_ended:
            game.stop()
            if game.won:
                print("end; going to \""+game.next_level+"\"")
                if game.next_level == "final":
                    sectionmgr.add(GameEndSection(True, game))
                    self.to_be_removed = True
                else:
                    full_level = level.load_tiled("res/maps/"+game.next_level+".tmx")
                    self.start_game(Game(full_level))
            else:
                print("end; going to end section")
                self.to_be_removed = True
                sectionmgr.add(GameEndSection(False, game))

class GameEndSection(Section):
    def __init__(self, won, game):
        Section.__init__(self)
        self.won = won
        self.game = game
        self.ticks = 0

        self.bgimage = pygame.Surface((SCREEN_W, SCREEN_H))
        self.bgimage.fill((255,255,255))
        if won:
            image = pygame.transform.scale(images.get("winbg"), (480, 245))
            self.bgimage.blit(image, (0,(SCREEN_H-245)//2))
        else:
            image = pygame.transform.scale(images.get("leikkaus2"), (480, 245))
            self.bgimage.blit(image, (0,(SCREEN_H-245)//2))

    def on_draw(self, screen):
        screen.blit(self.bgimage, (0,0))

        # Leave this thing on the screen as a hint to the player
        atlas = images.get("blood_transfusion")
        health_dst_rect = pygame.Rect(0, 0, 40, 56)
        health_dst_rect.center = (SCREEN_W-22, 30)
        health_sprite_id = 9
        health_src_rect = pygame.Rect(health_sprite_id * 40, 0, 40, 56)
        screen.blit(atlas, health_dst_rect, health_src_rect)

        return True
    
    def done(self):
        self.to_be_removed = True
        self.game.stop()

    def on_pygame_event(self, event):
        if event.type == pygame.KEYDOWN:
            self.done()
            return True

    def on_tick(self):
        self.ticks += 1
        if self.ticks > GAME_END_WAIT_S / TICK_LENGTH_S:
            self.done()


